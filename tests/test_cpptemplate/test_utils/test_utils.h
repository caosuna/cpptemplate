#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#include <utils.h>

using namespace utils;

void STRING_ASSERT_EQ(std::string received, std::string expected)
{
    ASSERT_EQ(received.length(), expected.length());
    for (int i = 0; i < received.length(); i++)
    {
        ASSERT_EQ(received[i], expected[i]);
    }
}

TEST(cpptemplate, make_string_says_hi)
{
    std::string received = make_string();
    std::string expected = "Hello world!\n";
    STRING_ASSERT_EQ(received, expected);
}

#endif
