#include "utils.h"
#include <absl/strings/str_join.h>
#include <string>

namespace utils
{
    std::string make_string(void)
    {
        std::string mystring = absl::StrJoin({"Hello", "world!\n"}, " ");
        return mystring;
    }

}