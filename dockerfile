FROM gcc:11.2 as base
SHELL ["/bin/bash", "-c"]

FROM base as dev
RUN apt-get update -y && apt-get install python3-pip -y
COPY scripts/pip pip
RUN pip install -r pip/requirements.txt && rm -rf pip

RUN conan remote add package_repo https://gitlab.com/api/v4/projects/26849328/packages/conan --insert 0
COPY scripts/conan/profile.toml /root/.conan/profiles/default
COPY conanfile.py .
RUN conan install . -if conan --build=missing && rm conanfile.py && rm -rf conan

FROM dev as build
COPY . cpptemplate
WORKDIR /cpptemplate
RUN conan install . -if build
RUN conan build . -bf build
RUN conan export-pkg . -sf src -bf build --ignore-dirty
RUN conan install cpptemplate/`python3 scripts/get_version.py`@ -if dist -g deploy

FROM base as prod
COPY --from=build cpptemplate/dist/cpptemplate /
ENTRYPOINT ["cpptemplate"]