"""Sends to console the setuptools_scm-defined version string from git."""
import setuptools_scm


def main():
    print(setuptools_scm.get_version(fallback_version="0.0.0"))


if __name__ == "__main__":
    main()
