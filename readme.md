# [C++ Template](https://gitlab.com/caosuna/cpptemplate/)
[![Version](https://img.shields.io/badge/version-0.0.0-green)]()
[![Language](https://img.shields.io/badge/C++-17-yellow)](https://en.cppreference.com/w/cpp/17)
[![License](https://img.shields.io/badge/license-MIT-gray)](https://opensource.org/licenses/MIT)

Example Template for C++ project.

## Overview
Template for a package-managed C++ project.

## Install
You can install directly from the package repository or build from source.

### Package Repository
The following sets a package repository as the default remote and installs a package. Be sure to set `$VERSION` for the installation.
```console
# conan remote add template_repo https://gitlab.com/api/v4/projects/26849328/packages/conan --insert 0
# conan install cpptemplate/$VERSION@ -if /usr --build=missing
```
- If you don't want to change the default package, remove `--insert 0` and add `-r template_repo` in `conan install` step

### From Code Repository
Git clone the repository and mount the directory to the `dockerfile` `dev` build stage to reproduce the dev environment.

Build:
```console
# conan install . -if build
# conan build . -bf build
```
- If you want to skip building tests, specify `src/CMakeLists.txt` as top-level by modifying `conan build` call to `conan build . -bf build -sf src`

Package & Deploy:
```console
# conan export-pkg . -bf build
# conan install cpptemplate/0.0.3@ -g deploy -if /
```

## Usage
```console
# cpptemplate
Hello world!
# test_cpptemplate
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from cpptemplate
[ RUN      ] cpptemplate.make_string_says_hi
[       OK ] cpptemplate.make_string_says_hi (0 ms)
[----------] 1 test from cpptemplate (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.
```
