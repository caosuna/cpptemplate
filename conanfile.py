from conans import ConanFile, CMake
import setuptools_scm


class CppTemplate(ConanFile):
    name = "cpptemplate"
    scm = {
        "type": "git",
        "url": "auto",
        "revision": "auto",
    }
    license = "MIT"
    description = "C++ Template"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    exports_sources = "src/*"

    generators = "cmake_find_package"
    requires = "abseil/20211102.0", "gtest/cci.20210126"
    build_requires = "cmake/3.25.0"

    def set_version(self):
        try:
            self.version = setuptools_scm.get_version(fallback_version="0.0.0")
        except:
            self.version = "0.0.0"

    def build(self):
        cmake = CMake(self)
        cmake.configure(
            source_folder=self.source_folder, build_folder=self.build_folder
        )
        cmake.build()
        if not self.source_folder.endswith("/src"):
            cmake.test()
        cmake.install()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        self.copy("*", src="bin", dst="bin")
        self.copy("*.h", dst="include", src="src", keep_path=False)
        self.copy("*.dll", dst="lib", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)

    def deploy(self):
        self.copy("*", src="bin", dst="bin")
        self.copy("*.h", dst="include", src="src", keep_path=False)
        self.copy("*.dll", dst="lib", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
